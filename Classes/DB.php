<?php


class DB
{
    private $mysqli;
    public $connect_error;
    public function __construct($host, $username, $psswd, $basename)
    {
        $this->mysqli = new mysqli($host, $username, $psswd, $basename);
        if($this->mysqli->connect_errno)
            $this->connect_error = true;
        else $this->connect_error = false;
    }

    public function query($query)
        //расширение для mysqli->query
        //возвращает ассоциативный массив
    {
        $result = $this->mysqli->query($query);
        if($result !== true)
        {
            while($tmp = $result->fetch_assoc())
            {
                $arr[] = $tmp;
            }
            return $arr;
        }
        else return $result;
    }

    public function genQuery($query)
        // аналог query, но работает как генератор
    {
        $dump = $this->mysqli->query($query);
        if($dump !== true)
        {
            while($result = $dump->fetch_assoc())
            {
                yield $result;
            }
        }
        else return $dump;
    }

    public function safeQuery($query, ...$params)
        //аналог prepare для mysqli, собранный сразу для выполнения запроса
        //1 параметр - сам запрос SQL
        //далее идет множество параметров для bind_param
    {
        $base = $this->mysqli->prepare($query);
        $values = "";
        foreach ($params as $item)
        {
            $values .= gettype($item)[0];
        }
        if(!$base->bind_param($values, ...$params))
            return false;
        if(!$base->execute())
            return false;
        $base = $base->get_result();
        if($base !== false)
        {
            while($result = $base->fetch_assoc())
            {
                $arr[] = $result;
            }
            return $arr;
        }
        else return $base;
    }

    public function genSafeQuery($query, ...$params)
        //аналог safeQuery, работающий как генератор
    {
        $values = "";
        $base = $this->mysqli->prepare($query);
        foreach ($params as $item)
        {
            $values .= gettype($item)[0];
        }
        if(!$base->bind_param($values, ...$params))
            return false;
        if(!$base->execute())
            return false;
        $base = $base->get_result();
        if($base !== false)
        {
            while($result = $base->fetch_assoc())
            {
                yield $result;
            }
        }
        else return $base;
    }


    public function __destruct()
    {
        $this->mysqli->close();
    }
}