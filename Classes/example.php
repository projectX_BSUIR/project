<?php


require_once "Pagination.php";
require_once "DB.php";


$base = new DB('localhost', 'admin', 'roman', 'mybase');
if($base->connect_error)
    echo "произошла ошибка при подключении к DB\n</br>\n";


$content = "id nomer ?[id]? i data content : ?[data]?</br>";
try{
    $pagination = new Pagination($base->query("SELECT * FROM test"), $content, 33);
}
catch (UnexpectedValueException $exception)
{
    echo $exception->getMessage();
    //redirect can be used!
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .page_tab, .active_page_tab{
            padding: 10px;
            background-color: darkblue;
            color: aqua;
            text-decoration: none;
            margin-right: 10px;
            border-radius: 50px;
        }

        .active_page_tab{
            background-color: blue;
        }

        .pagination_tab{
            height: 50px;
            margin-top: 50px;
            width: max-content;
        }

        .main_content{
            margin-left: 40%;
        }

    </style>
</head>
<body>
<div class="main_content">
    <?php
    if(isset($pagination)):
        foreach ($pagination->getPaginatedContent() as $note):?>
            <h3>
                <?php echo $note;?>
            </h3>
        <?php endforeach;
        echo $pagination->getPaginationTab("index.php");
    endif;
    ?>
</div>
</body>
</html>
