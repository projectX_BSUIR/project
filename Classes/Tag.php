<?php


class Tag
{
    private $tag_name;
    private $attrs;
    private $text;

    public function __construct($tag_name)
    {
        $this->tag_name = $tag_name;
    }

    public function SetText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function SetAttr($attrs)
    {
        foreach ($attrs as $attr => $value)
        {
            $this->attrs[$attr] = $value;
        }
        return $this;
    }


    public function getTagHTML()
    {
        $str = "<";
        $str .= $this->tag_name;
        foreach ($this->attrs as $attr => $value)
        {
            $str .= " $attr='$value'";
        }
        $str .= ">$this->text</$this->tag_name>";
        return $str . "\n";
    }
}