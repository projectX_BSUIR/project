<?php

require_once "Tag.php";

class Pagination
{
    private array $notes;
    private $content;
    private $current_page;
    private $matches;
    private $items_count;
    private $pages_count;
    private $items_on_page;
    private const REGEX = '{\\?\\[(\S+)\\]\\?}';
    public function __construct(array $notes, $content, $items_on_page = 20)
        // в консрукторе 1 параметром указываются все данные для записей,
        // по которым будет составлна пагинация, 2 параметром указывается
        // внешний вид каждой записи со выделенными местами под данные ?[smth]?
    {
        $this->items_on_page = $items_on_page;
        $this->notes = $notes;
        $this->content = $content;
        $this->items_count = count($this->notes);
        $this->pages_count = (($this->items_count % $this->items_on_page) > 0) ? (int)($this->items_count / $this->items_on_page) + 1 : $this->items_count / $this->items_on_page;
        if(isset($_GET['page']) && $_GET['page'] > $this->pages_count)
            throw new UnexpectedValueException('такой страницы не существует');
        $this->current_page = isset($_GET['page']) ? $_GET['page'] : 1;
        preg_match_all(Pagination::REGEX, $content, $this->matches);
    }


    public function getPaginatedContent()
        //используется как генератор, yield возвращает каждую запись
    {
        for($i = $this->items_on_page * ($this->current_page - 1); $i < $this->items_on_page * ($this->current_page); $i++)
        {
            $note = $this->content;
            if($i == $this->items_count)
                break;
            foreach ($this->matches[1] as $item)
            {
                $note = preg_replace('{\\?\\[('. $item . ')\\]\\?}', $this->notes[$i][$item], $note);
            }
            yield $note;
        }
    }

    public function getPaginationTab($href)
        //единственный параметр являтся адресом страниц для пагинации
        //возвращает div со всеми номерами страниц
    {
        for($i = 0;$i < $this->pages_count; $i++)
        {
            $tag[] = new Tag('a');
        }
        $result_tab = "<div class='pagination_tab'>\n";
        for($i = 0; $i < $this->pages_count; $i++)
        {
            if($i + 1 == $this->current_page)
            {
                $tag[$i]->SetText($i + 1)->SetAttr(["href"=>"$href?page=" . ($i + 1), "class"=>"active_page_tab"]);
                $result_tab .= $tag[$i]->getTagHTML();
                continue;
            }
            $tag[$i]->SetText($i + 1)->SetAttr(["href"=>"$href?page=" . ($i + 1), "class"=>"page_tab"]);
            $result_tab .= $tag[$i]->getTagHTML();
        }
        $result_tab .= "</div>\n</br>";
        return $result_tab;
    }
}